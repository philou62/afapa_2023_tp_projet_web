package testWebProject;

public class Compte {
	
	private int idCompte;
	private String login;
	private String pass;
	private String type = "s";
	int idUsers;
	
	public Compte(String login, String pass, int idUsers) {
		this.login = login;
		this.pass = pass;
		this.idUsers = idUsers;
		this.type = getType();
	}

	public int getIdUsers() {
		return idUsers;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pass;
	}

	public void setPwd(String pass) {
		this.pass = pass;
	}

	public int getIdCompte() {
		return idCompte;
	}

	public String getType() {
		return type;
	}
	
	
	

}
