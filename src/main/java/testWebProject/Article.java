package testWebProject;

public class Article {

	private String designation;
	private double prixUnitaire;
	private int quantite;
	private int idCategorie;

	public Article(String designation, double pu, int quantite, int idCategorie) {
		this.designation = designation;
		this.prixUnitaire = pu;
		this.quantite = quantite;
		this.idCategorie = idCategorie;
	}
	
	public Article() {
		
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public double getPrixUnitaire() {
		return prixUnitaire;
	}

	public void setPrixUnitaire(double prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public int getIdCategorie() {
		return idCategorie;
	}


}
