package testWebProject;

import java.sql.*;
import java.util.ArrayList;
import java.util.*;

public class Connect {

	private PreparedStatement ps; // pour les requêtes de mise à jour
	private Statement st; // pour les requêtes d'interrogations
	private ResultSet rs; // jeu de résultat
	String sql = "";
	Connection cn = null;

	public Connection etablirConnection() {

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			cn = DriverManager.getConnection("jdbc:mysql://localhost:3306/leBeauVelo", "root", "");
			st = cn.createStatement();
			if (cn != null) {
				System.out.println("Connexion ok !");
			} else {
				System.out.println("connexion ko !");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cn;
	}

	public void clotureConnect() {
		try {
			cn.close();
			st.close();
			System.out.println("Connection fermée!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String verifierCoordonnee(String login) {
		String mdp = null;
		etablirConnection();
		sql = "select pwd from compte where login like '" + login + "'";
		try {
			rs = st.executeQuery(sql);
			if (rs.next()) {
				mdp = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mdp;
	}

	public void ajouterCompte(Compte cp) throws SQLException {
		etablirConnection();
//		sql = "select * compte where login like '" + cp.getLogin() + "'";
//		rs = st.executeQuery(sql);
		// if(!rs.next()) {
		sql = "insert into compte(login, pwd, type, idUsers) values" + "('" + cp.getLogin() + "','" + cp.getPwd()
				+ "','" + cp.getType() + "'," + cp.getIdUsers() + ");";
		ps = cn.prepareStatement(sql);
		ps.execute();
		clotureConnect();
		// }
	}

	public void ajouterUsers(Users u) throws SQLException {
		etablirConnection();
		sql = "insert into users (fName, lName, adresse, tel, age, sexe) values('" + u.getfName() + "','" + u.getlName()
				+ "','" + u.getAdresse() + "','" + u.getTel() + "'," + u.getAge() + ",'" + u.getSexe() + "');";
		ps = cn.prepareStatement(sql);
		ps.execute();
		clotureConnect();
	}

	public int idLastUsers() throws SQLException {
		etablirConnection();
		int id = 0;
		sql = "select max(idUsers) from users;";
		rs = st.executeQuery(sql);
		if (rs.next()) {
			id = rs.getInt(1);
		}
		clotureConnect();
		return id;
	}

//	public List<Categorie> CategorieList() throws SQLException {
//		String designation = null;
//		List<Categorie> categories = new ArrayList<>();
//
//		sql = "SELECT id, designation FROM categorie";
//
//		rs = st.executeQuery(sql);
//
//		while (rs.next()) {
//			Categorie categorie = new Categorie(designation);
//			categorie.getDesignation();
//			categorie.setId(rs.getInt("id"));
//			categories.add(categorie);
//			for (Categorie categorie1 : categories) {
//				System.out.println(categorie1);
//			}
//		}
//		clotureConnect();
//		return categories;
//	}

	public List<String> listCat() {
		List<String> lcat = new ArrayList<String>();
		etablirConnection();
		String sql = "select * from categorie";
		try {
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				lcat.add(rs.getString(2));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lcat;
	}

	public boolean categorieExist(String designation) {
		etablirConnection();
		sql = "SELECT idCategorie FROM categorie WHERE designation LIKE '" + designation + "'";
		try {
			rs = st.executeQuery(sql);
			if (rs.next()) {
				clotureConnect();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		clotureConnect();
		return false;
	}

	public String ajoutCat(String cat) {
		String str = "";
		if (!categorieExist(cat)) {
			sql = "insert into categorie(designation) values('" + cat + "')";
			try {
				etablirConnection();
				st.executeUpdate(sql);
				str = "Ajout du catégorie " + cat + " réussie.";
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			str = "La catégorie " + cat + " existe déjà.";
		}
		clotureConnect();
		return str;
	}

	public void deleteCat(String choixCat) {
		sql = "delete * from categorie where designation LIKE '" + choixCat + "'";
		try {
			etablirConnection();
			st.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		clotureConnect();
	}

	public int findIdCategorie(String designation) throws SQLException {
		etablirConnection();
		int id = 0;
		sql = "SELECT idCategorie FROM categorie WHERE designation LIKE '" + designation + "'";
		rs = st.executeQuery(sql);
		if (rs.next()) {
			id = rs.getInt(1);
			clotureConnect();
		}
		return id;
	}

	public List<String> proListDelete() {
		List<String> lProDelete = new ArrayList<String>();
		etablirConnection();
		sql = "SELECT a.designation FROM article a, categorie c where a.idCategorie=c.idCategorie";
		try {
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				lProDelete.add(rs.getString(1));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lProDelete;
	}

	private boolean produitExist(Article prod) {
		etablirConnection();
		sql = "select idProduit from article where designation like '" + prod.getDesignation() + "'";
		try {
			rs = st.executeQuery(sql);
			if (rs.next()) {
				clotureConnect();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		clotureConnect();
		return false;
	}

	public void addPro(Article prod) throws SQLException {
		String str = "";
		if (!produitExist(prod)) {
			sql = "INSERT INTO article(designation, pu, qty, idCategorie) values ('" + prod.getDesignation() + "',"
					+ prod.getPrixUnitaire() + "," + prod.getQuantite() + "," + prod.getIdCategorie() + ")";
			etablirConnection();
			ps = cn.prepareStatement(sql);
			ps.execute();
			clotureConnect();
			str = "Article ajouter avec succés!";
		} else {
			str = "l'article eiste deja!";
		}
		clotureConnect();
	}
	
	public void deletePro(String choixPro) {
		sql = "delete * from article where designation LIKE '" + choixPro + "'";
		try {
			etablirConnection();
			st.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		clotureConnect();
	}

//	public static void main(String[] args) throws SQLException {
//		Connect c = new Connect();
//		
//		System.out.println(c.findIdCategorie("Electromenager")); 
//	}
}
