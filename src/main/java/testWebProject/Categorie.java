package testWebProject;

public class Categorie {
	
	private String designation;
	private int id;

	public Categorie(String designation) {
		this.designation = designation;
	}
	
	public String getDesignation() {
		return designation;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}
	
}
