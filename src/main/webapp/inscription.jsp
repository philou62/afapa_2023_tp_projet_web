<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.Date"%>
<%-- <%@ tablig uri = "http://java.sun.com/jsp/jstl/core" %> --%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/app.css">
<title>Le Beau Vélo</title>
</head>
<body>

	<jsp:include page="navbar.jsp"></jsp:include>

	<h2 class="text-center"><%=new Date()%></h2>
	<form action="LeServlet?flag=inscrit" method="POST">
		<fieldset>
			<legend class="text-center fs-1">Inscription</legend>
			<div class="container aqua">
				<p
					class="${empty erreurs ? 'succes' : 'erreur'} text-center fs-1 text-success bg-primary">${resultat}</p>
			</div>
			<hr>
			<div class="container">
				<div class="d-flex justify-content-center">
					<div class="row ">
						<div class="col-6 gap-3">
							<div class="form-group">
								<label for="adresse" class="form-label"></label> <select
									name="adresse" id="adresse">
									<option value="none" class="form-control">Sélectionnez
										un pays</option>
									<option value="tn" class="form-control">Tunisie</option>
									<option value="es" class="form-control">Espagne</option>
									<option value="fr" class="form-control">France</option>
									<option value="it" class="form-control">Italie</option>
								</select>
							</div>
						</div>
						<div class="col-6 mt-4">
							<div class="form-group">
								<input name="sexe" id="sexeH" type="radio" value="H" size="20"
									checked /> <label for="sexeH">Homme</label> <input name="sexe"
									id="sexeF" type="radio" value="F" size="20" /> <label
									for="sexeF">Femme</label>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-6">
						<div class="form-group">
							<label for="firstname" class="form-label mt-4">nom</label> <input
								type="text" id="firstName" name="firstName" class="form-control"
								placeholder="Renseigner votre nom" autofocus="autofocus">
						</div>
					</div>
					<div class="col-6">
						<div class="form-group">
							<label for="lastName" class="form-label mt-4">prénom</label> <input
								type="text" id="lastName" name="lastName" class="form-control"
								placeholder="Renseigner votre prénom">
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-6">
						<div class="form-group">
							<label class="form-label mt-4">Téléphone</label> <input
								type="tel" id="tel" name="telr" class="form-control"
								placeholder="Renseigner votre n° telephone">
						</div>
					</div>
					<div class="col-6">
						<div class="form-group">
							<label for="tel" class="form-label mt-4">Age</label> <input
								type="number" id="age" name="age" class="form-control"
								placeholder="Renseigner votre age">
						</div>
					</div>
				</div>


				<div class="row">
					<div class="col-4">
						<div class="form-group">
							<label for="login" class="form-label mt-4">Pseudo</label> <input
								type="text" id="login" name="login" class="form-control"
								placeholder="Renseigner votre login">
						</div>
					</div>
					<div class="col-4">
						<div class="form-group">
							<label for="pwd" class="form-label mt-4">Mot de passe</label> <input
								type="password" id="pwd" name="pwd" class="form-control"
								placeholder="Renseigner votre mot de passe">
						</div>
					</div>
					<div class="col-4">
						<div class="form-group">
							<label for="pwd2" class="form-label mt-4">Confirmation
								mot de passe</label> <input type="password" id="pwd2" name="pwd2"
								class="form-control" placeholder="Confirmez votre mot de passe">
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<div class="d-grid gap-1">
								<button type="submit" class="btn btn-success mt-5">Valider</button>
							</div>
						</div>
						<div class="col-6">
							<div class="d-grid gap-1">
								<button type="reset" class="btn btn-danger mt-5">Annuler</button>
							</div>
						</div>
					</div>

				</div>
			</div>
		</fieldset>
	</form>
	<script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
</body>
</html>