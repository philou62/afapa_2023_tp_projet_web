<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/app.css">
<title>Le Beau Vélo</title>
</head>
<body>
	<jsp:include page="navbar.jsp"></jsp:include>

	<%
	String login = request.getParameter("pseudo");
	String pwd = request.getParameter("mdp");
	if (login == null) {
		login = "";
	}
	if (pwd == null) {
		pwd = "";
	}

	if (request.getMethod().equals("POST") && login.equals("admin") && pwd.equals("admin")) {
	%>
	<h2 class="text-center text-primary fs-1 mt-5">
		Bienvenue
		<%=login%></h2>
	<%
	} else {
	%>

	<form action="LeServlet?flag=connect" method="POST">
		<fieldset>
			<legend class="text-center fs-1">Connexion</legend>
			<hr>
			<div class="container">
				<div class="row">
					<div class="col-6">
						<div class="form-group">
							<label for="exampleInputEmail1" class="form-label mt-4">Pseudo</label>
							<input type="text" class="form-control" id="pseudo" name="pseudo"
								placeholder="Enter votre pseudo">
						</div>
					</div>
					<div class="col-6">
						<div class="form-group">
							<label for="exampleInputPassword1" class="form-label mt-4">Password</label>
							<input type="password" class="form-control" id="mdp" name="mdp"
								placeholder="Entrez votre mot de passe">
						</div>
					</div>
				</div>

				<div class=row>
					<div class="col-6">
						<div class="d-grid gap-1">
							<button type="submit" class="btn btn-success mt-5">Valider</button>
						</div>
					</div>
						<div class="col-6">
							<div class="d-grid gap-1">
								<button type="reset" class="btn btn-danger mt-5">Annuler</button>
							</div>
						</div>
					</div>
				</div>
				</fieldset>
	</form>
	<%
	}
	%>
	<script src="js/bootstrap.bundle.min.js"></script>

</body>
</html>
