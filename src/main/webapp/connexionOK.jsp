<%@ page import="com.mysql.cj.x.protobuf.MysqlxNotice"%>
<%@ page import="testWebProject.Compte"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="testWebProject.Connect"%>
<%@ page import="java.util.*"%>
<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 --%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/app.css">
<title>Le Beau Vélo</title>
</head>
<body>
	<jsp:include page="navbar.jsp"></jsp:include>

	<%-- 	<c:out value="Hello" /> --%>

	<%
	String login = request.getParameter("pseudo");
	String pwd = request.getParameter("mdp");
	if (login == null) {
		login = "";
	}
	if (pwd == null) {
		pwd = "";
	}

	// 	if (request.getMethod().equals("POST") && login.equals("admin") && pwd.equals("admin")) {
	%>
	<h2 class="text-center text-dark fs-1 mt-5">
		Bienvenue
		<%=login%></h2>

	<!-- 	<div class=" container alert alert-danger" role="alert"> -->
	<!-- 	<h1 -->
	<%-- 		class="${msgReponse == '' ? 'erreur' : 'succes'} text-center text-dark">${msgReponse}</h1> --%>
	<!-- 	</div> -->

	<div class="container">
		<div class="row">
			<div class="col-6">
				<div class="form-group">
					<button onclick="afficherFormulaireCat()" id="ajoutCat"
						class="btn btn-dark mt-5 btn-lg text-success form-control">Ajouter
						une categorie</button>
				</div>
			</div>
			<div class="col-6">
				<div class="form-group">
					<button onclick="afficherFormDeleteCat()" id="suppCat"
						class="btn btn-dark mt-5 btn-lg text-danger form-control">Supprimer
						des categories</button>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-6">
				<div class="form-group">
					<button onclick="afficherFormulairePro()" id="ajoutPro"
						class="btn btn-dark mt-5 btn-lg text-success form-control">Ajouter
						des produits</button>
				</div>
			</div>
			<div class="col-6">
				<div class="form-group">
					<button onclick="afficherFormulaireSuppPro()" id="suppPro"
						class="btn btn-dark mt-5 btn-lg text-danger form-control">Supprimer
						un produit</button>
				</div>
			</div>

		</div>
	</div>

	<form id="formCat" method="POST" action="LeServlet?flag=adminAjoutCat"
		style="display: none">
		<fieldset>
			<legend class="text-center fs-1">Ajout une catégorie</legend>
			<hr>
			<div class="container">
				<div class="form-group">
					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<label for="cat" class="form-label mt-4">ajouter une
									categorie</label> <input type="text" id="cat" name="cat"
									class="form-control" placeholder="Ajouter une categorie"
									autofocus="autofocus">
							</div>
						</div>
						<div class="col-6">
							<div class="d-grid gap-1">
								<label> </label>
								<button type="submit" class="btn btn-success mt-5">ajouter</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
	<form id="formCatDeleteCat" method="POST"
		action="LeServlet?flag=adminSuppCat" style="display: none">
		<fieldset>
			<legend class="text-center fs-1">Supprimer une catégorie</legend>
			<hr>
			<div class="container">
				<div class="form-group">
					<div class="row">
						<div class="col-6">
							<label for="catListe" class="form-label mt-5">Supprimer</label>
							<%
							Connect cn = new Connect();
							List<String> list = new ArrayList<String>();
							list = cn.listCat();
							String item;
							%>
							<select name="catListe" id="catListe" class="form-control">
								<%
								for (int i = 0; i < list.size(); i++) {
									item = list.get(i);
								%>
								<option value=<%=item%>><%=item%></option>
								<%
								}
								%>
							</select>
						</div>

						<div class="col-6">
							<div class="d-grid gap-1">
								<button type="submit" class="btn btn-danger mt-4">Supprimer</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</form>

	<form id="formPro" method="POST" action="LeServlet?flag=adminAddPro"
		style="display: none">
		<fieldset>
			<legend class="text-center fs-1">Ajout des produits</legend>
			<hr>
			<div class="container">
				<div class="form-group">
					<div class="row">
						<div class="col-6">
							<label for="catList" class="form-label mt-4">Choisir une
								categorie</label>
							<%
							Connect c = new Connect();
							list = c.listCat();
							String items;
							%>

							<select name="catList" id="catList" class="form-control">
								<%
								for (int i = 0; i < list.size(); i++) {
									items = list.get(i);
								%>
								<option value=<%=items%>><%=items%></option>
								<%
								}
								%>
							</select>
						</div>
						<div class="col-6">
							<div class="form-group">
								<label for="addPro" class="form-label mt-4">nom du
									produits </label> <input type="text" id="addPro" name="addPro"
									class="form-control" placeholder="Ajouter un produit"
									autofocus="autofocus">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<label for="addQtePro" class="form-label mt-4">quantité</label>
								<input type="number" id="addQtePro" name="addQtePro"
									class="form-control" placeholder="Ajouter une Quantité">
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<label for="addPuPro" class="form-label mt-4">Prix
									unitaire</label> <input type="number" id="addPuPro" name="addPuPro"
									class="form-control" placeholder="Ajouter un prix">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<div class="d-grid gap-1">
								<button type="submit" class="btn btn-success mt-5">ajouter</button>
							</div>
						</div>
						<div class="col-6">
							<div class="d-grid gap-1">
								<button type="reset" class="btn btn-danger mt-5">Annuler</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
	<form id="formSuppPro" method="POST"
		action="LeServlet?flag=adminSuppPro" style="display: none">
		<fieldset>
			<legend class="text-center fs-1">Supression de produits</legend>
			<hr>
			<div class="container">
				<div class="form-group">
					<div class="row">
						<div class="col-6">
							<label for="catList" class="form-label mt-4">Choisir une
								categorie</label>
							<%
							Connect cnx = new Connect();
							list = cnx.listCat();
							String catListFromDeletePro;
							%>

							<select name="proListDelete" id="proListDelete"
								class="form-control">
								<%
								for (int i = 0; i < list.size(); i++) {
									catListFromDeletePro = list.get(i);
								%>
								<option value=<%=catListFromDeletePro%>><%=catListFromDeletePro%></option>
								<%
								}
								%>
							</select>
						</div>
						<div class="col-6">
							<label for="catList" class="form-label mt-4">Choisir un
								produit à supprimer</label>
							<%
							Connect cnxn = new Connect();
							List<String> listPro = new ArrayList<String>();
							listPro = cn.proListDelete();
							String deletePro;
							%>

							<select name="catList" id="catList" class="form-control">
								<%
								for (int i = 0; i < list.size(); i++) {
									deletePro = listPro.get(i);
								%>
								<option value=<%=deletePro%>><%=deletePro%></option>
								<%
								}
								%>
							</select>
						</div>
					</div>
				</div>
		</fieldset>
	</form>
	<script type="text/javascript" src="js/app.js"></script>
	<script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
</body>
</html>